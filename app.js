var express = require('express');
var path = require('path');
var logger = require('morgan');

var indexRouter = require('./routes/index');
var usuariosRouter = require('./routes/usuarios');

var app = express();
var mongoose=require('mongoose');


require('dotenv').config()


app.set('port',  process.env.PORT || port);


console.log(process.env.PORT)
console.log(process.env.DB_URI)

mongoose.set('useCreateIndex', true);
mongoose.connect(process.env.DB_URI,{ useNewUrlParser: true })
	.then(() => { 
		console.log('corriendo Base de datos :\x1b[32m%s\x1b[0m', 'online');
		
		
	}).
	catch((err) => {
	  throw err;
  });


// view engine setup
app.set('port', process.env.port || port);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, 'public')));


app.listen(port, () => {
    console.log(`Server running on port: ${port}`);
});



//RUTAS
app.use('/', indexRouter);
app.use('/usuarios', usuariosRouter);








// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
