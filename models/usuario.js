 //cargas Mongos
var mongoose=require('mongoose');
 //Definir esquema
var  Schema=mongoose.Schema;
// VALIDACIONES UNICAS sudo npm i mongoose-unique-validator --save
var uniqueValidator= require('mongoose-unique-validator');
//
var  rolesValido ={
    values:['ADMIN_ROLE','USER_ROLE'],
    message:'{VALUES} no es valido/permitido'
}
 //Esquema usuario
 var usuarioShema= new Schema({
    nombre:{ type:String,required:[true,'El nombre es necesario']},
    password:{ type:String,required:[true,'El password es necesario']},
    email:{ type:String,unique:true,required:[true,'El correo es necesario']},
    role:{ type:String,required:false,default:'USER_ROLE',enum:rolesValido}
 })


 usuarioShema.plugin(uniqueValidator,{message:'{PATH} debe ser unico'})

 module.exports=mongoose.model('Usuario',usuarioShema);
 